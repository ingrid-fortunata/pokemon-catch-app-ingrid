import React, { useState, useEffect } from "react";
//import "bootswatch/dist/yeti/bootstrap.min.css";

export default function ContactUs() {
  const [notes, setNotes] = useState(null);
  const [form, setForm] = useState({ fullName: "", description: "" });

  //SAME AS COMPONENT DIDMOUNT
  useEffect(() => {
    fetchNotes();
  }, []);

  const fetchNotes = async () => {
    const response = await fetch("/notes");
    const data = await response.json();
    setNotes(data);
  };

  const handleChangeForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const postNotes = async (data) => {
    await fetch("/notes", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };

  const validate = () => {
    let error = {};

    if (!form.fullName) {
      error.fullName = "Name is required";
    } else if (!form.description) {
      error.description = "Please input your message";
    }

    return error;
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    const errors = validate();

    if (Object.keys(errors).length === 0) {
      await postNotes(form);
      setForm({ fullName: "", description: "" });
    } else {
      showError(errors);
    }
    fetchNotes();
  };

  const showError = (errorObject) => {
    let errorMessage = "";
    for (let error in errorObject) {
      errorMessage += `${errorObject[error]}.`;
    }
    alert(`${errorMessage}`);
  };

  return (
    <div>
      <h1> Contact Us </h1>
      <div className="container" style={{ width: 400, marginTop: 20 }}>
        <form onSubmit={handleSubmitForm}>
          <fieldset>
            <div className="form-group">
              <label for="name">Name :</label>
              <input
                type="text"
                className="form-control"
                id="fullName"
                name="fullName"
                placeholder="Insert your full name"
                onChange={handleChangeForm}
              />
            </div>
            <div class="form-group">
              <label for="description"> Description </label>
              <textarea
                class="form-control"
                id="description"
                rows="5"
                name="description"
                placeholder="Insert your notes"
                onChange={handleChangeForm}
              />
            </div>
            <button type="submit" class="btn btn-primary">
              Submit
            </button>
          </fieldset>
        </form>
        {/* <div style={{ width: 400, marginTop: 20 }}>
          {JSON.stringify(notes, null, 8)}
        </div> */}
      </div>
    </div>
  );
}
